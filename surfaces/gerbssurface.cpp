#include "gerbssurface.h"

#include "psimplesubsurf.h"

template<typename T>
GERBSSurface<T>::GERBSSurface()
{

}

template<typename T>
GERBSSurface<T>::GERBSSurface(GMlib::PSurf<T, 3> *surf, int m, int n)
    : GMlib::PSurf<T,3>(), _surf(surf), _m(m), _n(n)
{
    if(isClosedU()) _m++;
    if(isClosedV()) _n++;

    _generateKnots(_m, _n);

    _localSurfs.setDim(_m, _n);
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            _localSurfs[i][j] = new PSimpleSubSurf<float>(surf,
                    _tu[i], _tu[i+2], _tu[i+1],
                    _tv[j], _tv[j+2], _tv[j+1]);
            _localSurfs[i][j]->toggleDefaultVisualizer();
            _localSurfs[i][j]->setCollapsed(true);
            _localSurfs[i][j]->sample(10, 10, 1, 1);
            this->insert(_localSurfs[i][j]);

            // Set varying colors to distinguish between different surfaces.
            double fac = (i * 1.0) / (_localSurfs.getDim1() * 1.0);
            double redFac = (j * 1.0) / (_localSurfs.getDim2() * 1.0);
            GMlib::Color col = GMlib::Color(redFac, fac, 1.0 - fac);
            _localSurfs[i][j]->setMaterial(GMlib::Material(col, col, col, 0.0f));
        }
    }
    if(isClosedU()) {
        for(int j = 0; j < _n; j++) {
            _localSurfs[_m-1][j] = _localSurfs[0][j];
        }
    }
    if(isClosedV()) {
        for(int i = 0; i < _m; i++) {
            _localSurfs[i][_n-1] = _localSurfs[i][0];
        }
    }
}

template<typename T>
void GERBSSurface<T>::setLocalSurfVisible(bool visible)
{
    for(int i = 0; i < _localSurfs.getDim1(); i++) {
        for(int j = 0; j < _localSurfs.getDim2(); j++) {
            _localSurfs[i][j]->setVisible(visible);
        }
    }
}

template<typename T>
void GERBSSurface<T>::eval(T u, T v, int d1, int d2, bool lu, bool lv) const
{
    this->_p.setDim(d1+1, d2+1);

    int i = _findIndex(u, _tu);
    int j = _findIndex(v, _tv);

    auto p00 = _localSurfs(i-1)(j-1)->evaluateParent(u, v, d1, d2);
    auto p01 = _localSurfs(i-1)(j)->evaluateParent(u, v, d1, d2);
    auto p10 = _localSurfs(i)(j-1)->evaluateParent(u, v, d1, d2);
    auto p11 = _localSurfs(i)(j)->evaluateParent(u, v, d1, d2);

    auto wi = _w(u, i, _tu);
    auto wj = _w(v, j, _tv);
    auto bu = _bFunction(wi);
    auto bv = _bFunction(wj);
    auto bud = _bDerivative(wi);
    auto bvd = _bDerivative(wj);

    auto& s00 =  p00[0][0],  s01 = p01[0][0],  s10 = p10[0][0],  s11 = p11[0][0];
    auto& su00 = p00[1][0], su01 = p01[1][0], su10 = p10[1][0], su11 = p11[1][0];
    auto& sv00 = p00[0][1], sv01 = p01[0][1], sv10 = p10[0][1], sv11 = p11[0][1];

    // Position
    this->_p[0][0] = (((1 - bu) * s00  + bu  * s10)  * (1 - bv)
                   +  ((1 - bu) * s01  + bu  * s11)  *      bv);

    // u-derivative
    this->_p[1][0] = (((  -bud) * s00  + bud * s10)  * (1 - bv)
                   +  ((  -bud) * s01  + bud * s11)  *      bv)
                   + (((1 - bu) * su00 + bu  * su10) * (1 - bv)
                   +  ((1 - bu) * su01 + bu  * su11) *      bv);

    // v-derivative
    this->_p[0][1] = (((1 - bu) * s00  + bu  * s10)  * (  -bvd)
                   +  ((1 - bu) * s01  + bu  * s11)  *     bvd)
                   + (((1 - bu) * sv00 + bu  * sv10) * (1 - bv)
                   +  ((1 - bu) * sv01 + bu  * sv11) *      bv);
}

template<typename T>
void GERBSSurface<T>::localSimulate(double dt)
{
//    for(int i = 0; i < _localSurfs.getDim1(); i++) {
//        for(int j = 0; j < _localSurfs.getDim2(); j++) {
//            _localSurfs[i][j]->rotate(GMlib::Angle(1.0 * dt), GMlib::Vector<float, 3>(0.0f, 1.0f, 0.0f));
//        }
//    }
    this->sample(32, 32, 1, 1);
}

template<typename T>
T GERBSSurface<T>::_w(T t, int i, const std::vector<T> &knots) const
{
    return (t - knots[i]) / (knots[i+1] - knots[i]);
}

template<typename T>
void GERBSSurface<T>::_generateKnots(int m, int n)
{
    T su = getStartPU();
    T eu = getEndPU();
    T dt = (eu - su) / (m - 1);
    _tu.resize(m+2);
    _tu[0] = _tu[1] = su;
    for(int i = 1; i < m - 1; i++) {
        _tu[i+1] = su + dt * i;
    }
    _tu[m] = _tu[m+1] = eu;
    if(isClosedU()) {
        _tu[0] = _tu[1] - (_tu[m] - _tu[m-1]);
        _tu[m+1] = _tu[m] + (_tu[2] - _tu[1]);
    }

    T sv = getStartPV();
    T ev = getEndPV();
    dt = (ev - sv) / (n - 1);
    _tv.resize(n+2);
    _tv[0] = _tv[1] = sv;
    for(int i = 1; i < n - 1; i++) {
        _tv[i+1] = sv + dt * i;
    }
    _tv[n] = _tv[n+1] = ev;
    if(isClosedV()) {
        _tv[0] = _tv[1] - (_tv[n] - _tv[n-1]);
        _tv[n+1] = _tv[n] + (_tv[2] - _tv[1]);
    }
}

template<typename T>
int GERBSSurface<T>::_findIndex(T t, const std::vector<T> &knots) const
{
    for(int i = 1; i < knots.size() - 2; i++) {
        if(t >= knots[i] && t < knots[i+1])
            return i;
    }
    return knots.size() - 3;
}

template<typename T>
T GERBSSurface<T>::_bFunction(T w) const
{
    return (3*pow(w, 2)-2*pow(w, 3));
}

template<typename T>
T GERBSSurface<T>::_bDerivative(T w) const
{
    return 6*w-6*pow(w,2);
}
