#ifndef GERBSSURFACE_H
#define GERBSSURFACE_H

#include <parametrics/gmpsurf.h>

template<typename T>
class GERBSSurface : public GMlib::PSurf<T,3>
{
    GM_SCENEOBJECT(GERBSSurface)
public:
    GERBSSurface();
    GERBSSurface(GMlib::PSurf<T,3>* surf, int m, int n);

    void setLocalSurfVisible(bool visible);

    bool isClosedU() const override { return _surf->isClosedU(); }
    bool isClosedV() const override { return _surf->isClosedV(); }

protected:
    void eval(T u, T v, int d1, int d2, bool lu = true, bool lv = true) const override;
    T getStartPU() const override { return _surf->getParStartU(); }
    T getEndPU() const override { return _surf->getParEndU(); }
    T getStartPV() const override { return _surf->getParStartV(); }
    T getEndPV() const override { return _surf->getParEndV(); }

    void localSimulate(double dt) override;

private:
    GMlib::PSurf<T,3>* _surf;
    GMlib::DMatrix<GMlib::PSurf<T,3>*> _localSurfs;
    int _m, _n;
    std::vector<T> _tu, _tv;

    T _w(T t, int i, const std::vector<T>& knots) const;
    void _generateKnots(int m, int n);
    int _findIndex(T t, const std::vector<T>& knots) const;
    T _bFunction(T w) const;
    T _bDerivative(T w) const;
};

#include "gerbssurface.cpp"

#endif // GERBSSURFACE_H
