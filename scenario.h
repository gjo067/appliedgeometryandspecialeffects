#ifndef SCENARIO_H
#define SCENARIO_H


#include "application/gmlibwrapper.h"

// qt
#include <QObject>
#include <core/utils/gmcolor.h>
#include <core/containers/gmdvector.h>




class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void    initializeScenario() override;
  void    cleanupScenario() override;

public slots:
  void    callDefferedGL();

private:
  void plotControlPoints(GMlib::DVector<GMlib::Vector<float, 3>> c,
                         GMlib::Color pointsColor = GMlib::Color(1.0, 1.0, 0.0),
                         GMlib::Color linesColor = GMlib::Color(0.0, 1.0, 0.0));

  // Examples
  void _modelCurve(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _bSpline(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _bSplineLeastSquares(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _blendingSpline(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _gerbsCurve(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _specialEffects(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _gerbsSurfaceOpen(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _gerbsSurfaceSphere(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
  void _gerbsSurfaceTorus(GMlib::Vector<float,3> offset = {0.0f,0.0f,0.0f});
};


#endif // SCENARIO_H
