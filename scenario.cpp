
#include <iostream>

#include "scenario.h"

// hidmanager
#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <scene/light/gmpointlight.h>
#include <scene/sceneobjects/gmpathtrack.h>
#include <scene/sceneobjects/gmpathtrackarrows.h>

#include <parametrics/curves/gmpbutterfly.h>
#include "curves/figureeightknot.h"
#include "curves/mybspline.h"
#include "curves/blendedcurves.h"
#include "curves/gerbscurve.h"
#include "surfaces/gerbssurface.h"

#include <parametrics/surfaces/gmpsphere.h>
#include <parametrics/curves/gmpbsplinecurve.h>
#include <parametrics/curves/gmpline.h>
#include <parametrics/curves/gmpbeziercurve.h>
#include <parametrics/surfaces/gmpbeziercurvesurf.h>
#include <parametrics/visualizers/gmpsurfnormalsvisualizer.h>
#include <parametrics/surfaces/gmpcylinder.h>
#include <parametrics/surfaces/gmptorus.h>

// qt
#include <QQuickItem>

using vec3 = GMlib::Vector<float,3>;


template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << v.size() << std::endl;
  for(uint i=0; i<v.size(); i++) out << " " << v[i];
  out << std::endl;
  return out;
}


void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8f, 0.002f, 0.0008f);
  this->scene()->insertLight( light, false );

  // Insert Sun
  this->scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3>  init_cam_pos( 0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  1.0f, 0.0f, 0.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

    //------------- Model Curve
    _modelCurve();

    //--------------- BSpline
//    _bSpline();

    //-------------- Least-Squares BSpline
//    _bSplineLeastSquares();

    //---------------- Blending two curves
//    _blendingSpline();

    //---------------- GERBS
//    _gerbsCurve();
//    _specialEffects();

    //----------------- GERBS Surface
//    _gerbsSurfaceOpen();
//    _gerbsSurfaceSphere();
//    _gerbsSurfaceTorus();
}

void Scenario::cleanupScenario() {

}

void Scenario::callDefferedGL() {

  GMlib::Array< const GMlib::SceneObject*> e_obj;
  this->scene()->getEditedObjects(e_obj);

  for(int i=0; i < e_obj.getSize(); i++)
      if(e_obj(i)->isVisible()) e_obj[i]->replot();
}

void Scenario::plotControlPoints(GMlib::DVector<GMlib::Vector<float, 3>> c,
                                 GMlib::Color pointsColor, GMlib::Color linesColor)
{
    for(int i = 0; i < c.getDim(); i++) {
        auto s = new GMlib::PSphere<float>(0.2f);
        s->getMaterial().setDif(pointsColor);
        s->translate(c[i]);
        s->toggleDefaultVisualizer();
        s->sample(32, 32, 0, 0);
        scene()->insert(s);

        if(i > 0) {
            auto l = new GMlib::PLine<float>(GMlib::Point<float, 3>(c[i-1]), GMlib::Point<float, 3>(c[i]));
            l->setColor(linesColor);
            l->toggleDefaultVisualizer();
            l->sample(2, 0);
            scene()->insert(l);
        }
    }
}

void Scenario::_gerbsSurfaceOpen(GMlib::Vector<float, 3> offset)
{
    GMlib::DVector<GMlib::Vector<float, 3>> c;
    c.push_back(GMlib::Vector<float, 3>(0, 10, 0));
    c.push_back(GMlib::Vector<float, 3>(5, 2, 0));
    c.push_back(GMlib::Vector<float, 3>(10, 0, 0));
    c.push_back(GMlib::Vector<float, 3>(15, 2, 0));
    c.push_back(GMlib::Vector<float, 3>(20, 10, 0));
    c.push_back(GMlib::Vector<float, 3>(25, -5, 0));
    auto gmbs = new GMlib::PBSplineCurve<float>(c, 2, false);
    gmbs->toggleDefaultVisualizer();
    gmbs->sample(50, 1);
    gmbs->translate(offset);
    scene()->insert(gmbs);
    auto gmbs2 = new GMlib::PBSplineCurve<float>(c, 2, false);
    gmbs2->rotate(GMlib::Angle(90), GMlib::Vector<float, 3>(0.0f, 1.0f, 0.0f));
    gmbs2->toggleDefaultVisualizer();
    gmbs2->sample(50, 1);
    gmbs2->translate(offset);
    scene()->insert(gmbs2);

    auto modelSurface = new GMlib::PBezierCurveSurf<float>(gmbs, gmbs2);
    modelSurface->toggleDefaultVisualizer();
    modelSurface->sample(50, 50, 1, 1);
    modelSurface->translate(offset);
//    scene()->insert(modelSurface);

    auto gerbssurf = new GERBSSurface<float>(modelSurface, 3, 4);
    gerbssurf->toggleDefaultVisualizer();
    gerbssurf->sample(32, 32, 1, 1);
    gerbssurf->translate(offset);
    scene()->insert(gerbssurf);
//    gerbssurf->setLocalSurfVisible(false);

    auto normal = new GMlib::PSurfNormalsVisualizer<float, 3>();
    normal->setSize(0.3f);
    gerbssurf->insertVisualizer(normal);
}

void Scenario::_gerbsSurfaceSphere(GMlib::Vector<float, 3> offset)
{
    auto sphere = new GMlib::PSphere<float>(2.0f);
    sphere->translate(offset);
    sphere->sample(32, 32, 1, 1);

    auto gerbssurf = new GERBSSurface<float>(sphere, 3, 4);
    gerbssurf->toggleDefaultVisualizer();
    gerbssurf->sample(32, 32, 1, 1);
    gerbssurf->translate(offset);
    scene()->insert(gerbssurf);
//    gerbssurf->setLocalSurfVisible(false);

    auto normal = new GMlib::PSurfNormalsVisualizer<float, 3>();
    normal->setSize(0.3f);
    gerbssurf->insertVisualizer(normal);
}

void Scenario::_gerbsSurfaceTorus(GMlib::Vector<float, 3> offset)
{
    auto torus = new GMlib::PTorus<float>(3.0f, 1.0f, 1.0f);
    torus->translate(offset);
    torus->sample(32, 32, 1, 1);

    auto gerbssurf = new GERBSSurface<float>(torus, 3, 4);
    gerbssurf->toggleDefaultVisualizer();
    gerbssurf->sample(32, 32, 1, 1);
    gerbssurf->translate(offset);
    scene()->insert(gerbssurf);
//    gerbssurf->setLocalSurfVisible(false);

    auto normal = new GMlib::PSurfNormalsVisualizer<float, 3>();
    normal->setSize(0.3);
    gerbssurf->insertVisualizer(normal);
}

void Scenario::_gerbsCurve(GMlib::Vector<float, 3> offset)
{
    auto something = new FigureEightKnot<float>(2.0f);
    something->sample(200, 0);
    auto gerbs = new GERBSCurve<float>(something, 12);
    gerbs->toggleDefaultVisualizer();
    gerbs->sample(200, 0);
    gerbs->translate(offset);
    gerbs->doSpecialEffects(false);
    gerbs->setCollapsed(false);
//    gerbs->showOffsetLocalCurves();
    scene()->insert(gerbs);
    //    gerbs->setLocalCurvesVisible(false);
}

void Scenario::_specialEffects(GMlib::Vector<float, 3> offset)
{
    auto something = new FigureEightKnot<float>(2.0f);
    something->sample(200, 0);
    auto gerbs = new GERBSCurve<float>(something, 12);
    gerbs->toggleDefaultVisualizer();
    gerbs->sample(200, 0);
    gerbs->translate(offset);
    gerbs->doSpecialEffects(true);
//    gerbs->showOffsetLocalCurves();
    scene()->insert(gerbs);
    //    gerbs->setLocalCurvesVisible(false);
}

void Scenario::_blendingSpline(GMlib::Vector<float, 3> offset)
{
    GMlib::DVector<GMlib::Vector<float,3>> c1;
    c1.push_back(vec3(-20, 10, 0));
    c1.push_back(vec3(-10, 5, 0));
    c1.push_back(vec3(3, 5, 0));
    c1.push_back(vec3(7, -5, 0));
    auto bc1 = new MyBSpline<float>(c1);
    bc1->toggleDefaultVisualizer();
    bc1->sample(30, 0);
    bc1->setColor(GMlib::Color(255, 255, 0));
    bc1->showSelectors(0.3f, GMlib::Color(255, 255, 255), GMlib::Color(0, 0, 0));
    bc1->translate(offset);
    scene()->insert(bc1);

    GMlib::DVector<GMlib::Vector<float,3>> c2;
    c2.push_back(vec3(-2, 0, 0));
    c2.push_back(vec3(3, -20, 0));
    c2.push_back(vec3(10, -20, 0));
    c2.push_back(vec3(20, -10, 0));
    auto bc2 = new MyBSpline<float>(c2);
    bc2->toggleDefaultVisualizer();
    bc2->sample(30, 0);
    bc2->setColor(GMlib::Color(255, 0, 255));
    bc2->showSelectors(0.3f, GMlib::Color(255, 255, 255), GMlib::Color(0, 0, 0));
    bc2->translate(offset);
    scene()->insert(bc2);

    auto blended = new BlendedCurves<float>(bc1, bc2, 0.2f);
    blended->toggleDefaultVisualizer();
    blended->sample(50, 0);
    scene()->insert(blended);

    auto blended2 = new BlendedCurves<float>(bc1, bc2, 0.5f);
//    blended->linear = true;
    blended2->toggleDefaultVisualizer();
    blended2->sample(50, 0);
    blended2->setColor(GMlib::Color(0, 255, 0));
    scene()->insert(blended2);

    auto blended3 = new BlendedCurves<float>(bc1, bc2, 1.0f);
    blended3->toggleDefaultVisualizer();
    blended3->sample(50, 0);
    blended3->setColor(GMlib::Color(0, 0, 255));
    scene()->insert(blended3);

//    plotControlPoints(c1, GMlib::Color(1.0, 0.0, 0.0), GMlib::Color(0.0, 0.0, 1.0));
//    plotControlPoints(c2, GMlib::Color(0.0, 1.0, 1.0), GMlib::Color(0.0, 1.0, 0.0));
}

void Scenario::_bSplineLeastSquares(GMlib::Vector<float, 3> offset)
{
    auto something = new FigureEightKnot<float>(3.0f);
    something->sample(200, 0);
    something->toggleDefaultVisualizer();
    something->setColor(GMlib::Color(0, 0, 255));
    scene()->insert(something);
    GMlib::DVector<GMlib::Vector<float, 3>> p;
    int n = 200;
    for(int i = 0; i < n; i++) {
        p.push_back(something->evalAtT((i * 1.0f / n * 1.0f) * something->getMaxT()));
    }

    auto bs = new MyBSpline<float>(p, 30);
    bs->toggleDefaultVisualizer();
    bs->sample(200, 0);
    bs->showSelectors(0.2f, GMlib::Color(255, 0, 255), GMlib::Color(255, 255, 0));
    bs->translate(offset);
    scene()->insert(bs);
}

void Scenario::_bSpline(GMlib::Vector<float, 3> offset)
{
    GMlib::DVector<GMlib::Vector<float, 3>> c;
    c.push_back(GMlib::Vector<float, 3>(0, 10, 0));
    c.push_back(GMlib::Vector<float, 3>(5, 2, 0));
    c.push_back(GMlib::Vector<float, 3>(10, 0, 0));
    c.push_back(GMlib::Vector<float, 3>(15, 2, 0));
    c.push_back(GMlib::Vector<float, 3>(20, 10, 0));
    c.push_back(GMlib::Vector<float, 3>(25, -5, 0));
    auto bs = new MyBSpline<float>(c);
    bs->toggleDefaultVisualizer();
    bs->sample(50, 0);
    bs->showSelectors(0.2f, GMlib::Color(255, 0, 255), GMlib::Color(255, 255, 0));
    bs->translate(offset);
    scene()->insert(bs);
}

void Scenario::_modelCurve(GMlib::Vector<float, 3> offset)
{
    auto something = new FigureEightKnot<float>(3.0f);
    something->toggleDefaultVisualizer();
    something->setColor(GMlib::Color(0, 0, 255));
    something->sample(200, 0);
    something->translate(offset);
    scene()->insert(something);
}

