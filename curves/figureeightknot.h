#ifndef FIGURE_EIGHT_KNOT_H
#define FIGURE_EIGHT_KNOT_H

#include <parametrics/gmpcurve.h>

template<typename T>
class FigureEightKnot : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(FigureEightKnot)
public:
    FigureEightKnot(T size = T(5));

    bool isClosed() const override;

    GMlib::Vector<float, 3> evalAtT(float t) const;
    T getMaxT() {return getEndP();}

protected:
    void localSimulate(double dt) override;
    void eval(T t, int d, bool l) const override;
    T getStartP() const override;
    T getEndP() const override;

private:
    T _size;
    T _offset;
};

template<typename T>
inline
FigureEightKnot<T>::FigureEightKnot(T size) : GMlib::PCurve<T, 3>(20, 0, 0), _size(size), _offset(0) {}

template<typename T>
bool FigureEightKnot<T>::isClosed() const { return true; }

template<typename T>
void FigureEightKnot<T>::localSimulate(double dt)
{
    _offset += dt;
    //sample(200, 0);
}

template<typename T>
void FigureEightKnot<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);

    this->_p[0] = evalAtT(t);
}

template<typename T>
GMlib::Vector<float, 3> FigureEightKnot<T>::evalAtT(float t) const {
    T offset = cos(_offset);
    const double c2tp2 = 2 + cos(offset*2*t);
    const double c3t = cos(offset*3*t);
    const double s3t = sin(offset*3*t);
    const double s4t = sin(offset*4*t);

    GMlib::Vector<float, 3> p;
    p[0] = _size * c2tp2*c3t;
    p[1] = _size * c2tp2*s3t;
    p[2] = _size * s4t;
    return p;
}

template<typename T>
T FigureEightKnot<T>::getStartP() const {
    return T(0);
}

template<typename T>
T FigureEightKnot<T>::getEndP() const {
    return T(2 * M_PI);
}

#endif // FIGURE_EIGHT_KNOT_H
