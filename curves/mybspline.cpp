#include "mybspline.h"

#include <scene/selector/gmselector.h>
#include <scene/visualizers/gmselectorgridvisualizer.h>

#include <iostream>

template<typename T>
MyBSpline<T>::MyBSpline(const GMlib::DVector<GMlib::Vector<float, 3> > &c)
    : GMlib::PCurve<T, 3>(20, 0, 0), _n(c.getDim()), _degree(2),
      _order(_degree + 1), _controlPoints(c), _s(), _sgv(nullptr)
{
    _generateKnots();
}

template<typename T>
MyBSpline<T>::MyBSpline(const GMlib::DVector<GMlib::Vector<float, 3> > &p, int n)
    : GMlib::PCurve<T, 3> (20, 0, 0), _n(n), _degree(2),
      _order(_degree + 1), _s(), _sgv(nullptr)
{
    _generateKnots();
    _calculateControlPoints(p);
    if(isClosed()) {
        std::cout << "Test" << std::endl;
    }
}

template<typename T>
bool MyBSpline<T>::isClosed() const
{
    return false;
}

template<typename T>
void MyBSpline<T>::showSelectors(float radius, GMlib::Color selectorColor, GMlib::Color gridColor)
{
    if(_s.empty()) {
        _s.resize(_controlPoints.getDim());
        for(int i = 0; i < _controlPoints.getDim(); i++) {
            this->insert(_s[i] = new GMlib::Selector<T,3>(
                             _controlPoints[i], i, this));
            _s[i]->removeVisualizer(_s[i]->getVisualizers()[0]);
            _s[i]->insertVisualizer(new GMlib::SelectorVisualizer(
                radius, GMlib::Material(selectorColor*0.25, selectorColor*0.7, selectorColor, 10.0f)));
        }
    }

    if(!_sgv) {
        _sgv = new GMlib::SelectorGridVisualizer<T>;
        _sgv->setSelectors(_controlPoints);
        _sgv->setColor(gridColor);
        GMlib::SceneObject::insertVisualizer(_sgv);
        this->setEditDone();
    }
}

template<typename T>
void MyBSpline<T>::eval(T t, int d, bool /*l*/) const
{
    this->_p.setDim(d+1);

    int i = _findIndex(t);
    auto T2 = _calculateT2(i, t);
    this->_p[0] = T2[0] * _controlPoints[i - 2] + T2[1] * _controlPoints[i - 1]
                + T2[2] * _controlPoints[i];
}

template<typename T>
T MyBSpline<T>::getStartP() const
{
    return _t[_degree];
}

template<typename T>
T MyBSpline<T>::getEndP() const
{
    return _t[_n];
}

template<typename T>
void MyBSpline<T>::localSimulate(double dt)
{
    this->sample(200, 0);
}

template<typename T>
T MyBSpline<T>::_w(T t, int d, int i) const
{
    return (t - _t[i]) / (_t[i + d] - _t[i]);
}

template<typename T>
void MyBSpline<T>::_generateKnots()
{
    _t.resize(0);
    // Add 3 0s to the start of the knot vector
    for(int i = 0; i <= _degree; i++) {
        _t.push_back(0);
    }
    // Add n - degree increasing integers to the knot vector
    for(int i = 1; i < _n - _degree; i++) {
        _t.push_back(i);
    }
    // Add 3 (n - degree)'s to the end of the knot vector
    for(int i = 0; i <= _degree; i++) {
        _t.push_back(_n - _degree);
    }
    // Knot vector is now:
    // _t = {0, 0, 0, 1, 2, ..., (n-d-1), (n-d), (n-d), (n-d)}
}

template<typename T>
void MyBSpline<T>::_calculateControlPoints(const GMlib::DVector<GMlib::Vector<T,3>>& p)
{
    /* We have: p = array of points to estimate, in sequential order, length m
     *          _n = number of control points to create
     *          _d = degree of b-spline = 2
     * Where m > n
     *
     * We know: c(t) = T^2(t,i) * c
     * Where T^2(t,i) is the hard-coded basis for b-spline of order 2
     * If we now put t = x0, where x0 is the first element of a vector of length m
     *      where the elements are uniformly distributed between the start and end
     *      of the parameterization.
     * We then say that c(x0) = T^2(x0, i) * c = p0
     *      Where i is the index of the knot vector such that _t[i] <= x0 < _t[i+d]
     * Similarily for the rest of x = {x0,...,xm} and p = {p0,...,pm}.
     * Setting it up as a matrix gives us:
     * | T^2(x0,i)[0] T^2(x0,i)[1] T^2(x0,i)[2] ... 0 0 | | c1 |   | p0 |
     * | 0 T^2(x1,i)[0] T^2(x1,i)[1] T^2(x1,i)[2] ... 0 | | c2 | = | p1 |
     * |                  ...                           | | .. |   | .. |
     * | 0 ... t^2(xm,i)[0] t^2(xm,i)[1] t^2(xm,i)[2]   | | cn |   | pm |
     *
     * Denote the matrix as B, dim=3*m, control point vector as c, dim = n*1
     * and points p, dim m*1
     *
     * We want to solve for c, but B cannot be inversed.
     * To fix this, multiply both sides by B transposed, this gives us:
     * A*c = B^T*p, where A=B^T*B
     * We know that A is inversible, thus we get:
     * c = A^-1*B^T*p
     *
     * Now we have our control points of a B-spline which approximates
     * The given set of points p.
     */

    // B _c = p
    int m = p.getDim();
    float x_m = getEndP();
    GMlib::DMatrix<float> B(m, _n, 0.0f);
    for(int j = 0; j < m; j++) {
        float x = (j * 1.0f / m * 1.0f) * x_m;
        int i = _findIndex(x);
        auto T2 = _calculateT2(i, x);
        B[j][i-2] = T2[0];
        B[j][i-1] = T2[1];
        B[j][i] = T2[2];
    }

    auto B_t(B);
    B_t.transpose();
    auto A = B_t * B;
    A.invert();
    _controlPoints = A * B_t * p;
}

template<typename T>
GMlib::Vector<float, 3> MyBSpline<T>::_calculateT2(int i, T t) const
{
    /* T^2(t,i) = T_1(w(t)) * T_2(w(t))
     *
     * T^2(t,i) = | 1-w_1,i(t) w_1,i(t) | * | 1-w_2,i-1(t) w_2,i-1(t)    0     |
     *                                      |      0       1-w_w,i(t) w_2,i(t) |
     */
    auto w_1_i = _w(t, 1, i);
    auto w_2_i = _w(t, 2, i);
    auto w_2_i_1 = _w(t, 2, i - 1);

    GMlib::Vector<float, 3> T2;
    T2[0] = (1 - w_1_i) * (1 - w_2_i_1);
    T2[1] = (1 - w_1_i) * w_2_i_1 + w_1_i * (1 - w_2_i);
    T2[2] = w_1_i * w_2_i;

    return T2;
}

template<typename T>
int MyBSpline<T>::_findIndex(T t) const
{
    for(int i = _degree; i <= _t.size() - _order; i++) {
        if(t >= _t[i] && t < _t[i + 1]) {
            return i;
        }
    }
    return _n - 1;
}
