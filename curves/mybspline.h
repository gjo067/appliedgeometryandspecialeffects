#ifndef MYBSPLINE_H
#define MYBSPLINE_H

#include <parametrics/gmpcurve.h>
#include <vector>

namespace GMlib {
    template<typename T, int n>
    class Selector;

    template<typename T>
    class SelectorGridVisualizer;
}

template<typename T>
class MyBSpline : public GMlib::PCurve<T, 3>
{
    GM_SCENEOBJECT(MyBSpline)
public:
    // Use c as control points - and generate a knotvector
    MyBSpline(const GMlib::DVector<GMlib::Vector<float,3>>& c);
    // Use least square to make n control points from set of points p - and generate a knotvector
    MyBSpline(const GMlib::DVector<GMlib::Vector<float,3>>& p, int n);

    // Returns true if the curve is closed, always false
    bool isClosed() const override;
    // Returns a vector of control points
    GMlib::DVector<GMlib::Vector<float, 3>> getControlPoints() { return _controlPoints; }
    void showSelectors(float radius, GMlib::Color selectorColor, GMlib::Color gridColor);

protected:
    // Evaluate the position of the curve at time t
    // d = number of derivatives to calculate, none are implemented
    // l = calculate from left?, not used.
    void eval(T t, int d, bool l) const override;
    // Start of the parameterization
    T getStartP() const override;
    // End of the parameterization
    T getEndP() const override;
    void localSimulate(double dt) override;

private:
    int _n; // Number of control points
    int _degree; // Degree of the B-spline, hard-coded to 2 in this case.
    int _order; // Order of B-spline, order = degree + 1
    GMlib::DVector<GMlib::Vector<T,3>> _controlPoints; // vector of coordinates for control points
    std::vector<int> _t; // Knot vector
    GMlib::SelectorGridVisualizer<T>* _sgv; // Selector Grid
    std::vector<GMlib::Selector<T,3>*> _s; // Selectors

    // Function for scaling t from [t_i,t_i+d] to [0,1]
    T _w(T t, int d, int i) const;
    // Function to generate knots, clamped and uniformly distributed
    void _generateKnots();
    // Function to create control points based on a set of points, using least-squares
    void _calculateControlPoints(const GMlib::DVector<GMlib::Vector<T,3>>& p);
    // Hard-coded calculation of basis functions
    GMlib::Vector<float, 3> _calculateT2(int i, T t) const;
    // Finds the index i to _t based on current t such that t_i <= t < t_i+d
    int _findIndex(T t) const;
};

#include "mybspline.cpp"

#endif // MYBSPLINE_H
