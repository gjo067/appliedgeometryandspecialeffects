#include "blendedcurves.h"

#include <iostream>

template<typename T>
BlendedCurves<T>::BlendedCurves(GMlib::PCurve<T, 3> *c1, GMlib::PCurve<T, 3> *c2,
                                float factor)
    : _c1(c1), _c2(c2), _factor(factor)
{
    // Start and end of the parameterizations of the two curves
    float s1 = _c1->getParStart();
    float e1 = _c1->getParEnd();
    float s2 = _c2->getParStart();
    float e2 = _c2->getParEnd();
    // The lengths of the curves parameterization
    _l1 = e1 - s1;
    _l2 = e2 - s2;

    // Knot vector
    // example for factor = 0.2
    // _t = {0, 0.8, 1, 1.8}
    // Use curve 1 for t in [0, 0.8]
    // Use curve 2 for t in [1, 1.8]
    // Blend curves for t in [0.8, 1]
    _t = {0, 1 - factor, 1, 2 - factor};
}

template<typename T>
bool BlendedCurves<T>::isClosed() const
{
    return false;
}

template<typename T>
void BlendedCurves<T>::localSimulate(double dt)
{
    this->sample(200, 0);
}

template<typename T>
void BlendedCurves<T>::eval(T t, int d, bool l) const
{
    this->_p.setDim(d + 1);

    if(t <= _t[1]) {
        // Set p = to the evaluation of curve 1
        this->_p[0] = _c1->evaluateParent(t * _l1, d)[0];
    }
    else if(t >= _t[2]) {
        // Set p = to the evaluation of curve 2
        this->_p[0] = _c2->evaluateParent((t - (1 - _factor)) * _l2, d)[0];
    }
    else {
        // Evaluate curves
        auto p1 = _c1->evaluateParent(t * _l1, d)[0];
        auto p2 = _c2->evaluateParent((t - (1 - _factor)) * _l2, d)[0];

        // t3 is t scaled from [t1,t2] to [0,1]
        float t3 = (t - _t[1]) / (_t[2] - _t[1]);

        // Blend
        if(linear)
            this->_p[0] = (1-t3)*p1 + t3*p2;
        else
            this->_p[0] = (1-_bFunction(t3))*p1 + _bFunction(t3)*p2;
    }
}

template<typename T>
T BlendedCurves<T>::getStartP() const
{
    return _t[0];
}

template<typename T>
T BlendedCurves<T>::getEndP() const
{
    return _t.back();
}

template<typename T>
T BlendedCurves<T>::_bFunction(T t) const
{
    return 3*pow(t,2) - 2*pow(t,3);
}
