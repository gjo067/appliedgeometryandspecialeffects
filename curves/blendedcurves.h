#ifndef BLENDEDCURVES_H
#define BLENDEDCURVES_H

#include <parametrics/gmpcurve.h>

template<typename T>
class BlendedCurves : public GMlib::PCurve<T, 3>
{
    GM_SCENEOBJECT(BlendedCurves)
public:
    BlendedCurves(GMlib::PCurve<T, 3>* c1, GMlib::PCurve<T, 3>* c2, float factor);

    bool isClosed() const override;
    bool linear = false;

protected:
    void localSimulate(double dt) override;
    void eval(T t, int d, bool l) const override;
    T getStartP() const override;
    T getEndP() const override;

private:
    GMlib::PCurve<T, 3>* _c1;
    GMlib::PCurve<T, 3>* _c2;
    float _l1, _l2;
    float _factor;
    std::vector<float> _t;

    T _bFunction(T t) const;
};

#include "blendedcurves.cpp"

#endif // BLENDEDCURVES_H
