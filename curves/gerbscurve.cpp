#include "gerbscurve.h"

#include <core/utils/gmrandom.h>

template<typename T>
GERBSCurve<T>::GERBSCurve()
{

}

template<typename T>
GERBSCurve<T>::GERBSCurve(GMlib::PCurve<T, 3> *curve, int n)
    : GMlib::PCurve<T,3>(), _curve(curve), _numCurves(n), _isClosed(curve->isClosed())
{
    // If isClosed, add a new curve that is just the same as the first.
    if(_isClosed) {
        _numCurves++;
    }

    // Generate knot vector
    _generateKnots(_numCurves);

    // Make local curves
    _localCurves.resize(_numCurves);
    _origPoses.resize(_numCurves);
    for(int i = 0; i < n; i++) {
        _localCurves[i] = new GMlib::PSubCurve<T>(_curve, _t[i], _t[i+2], _t[i+1]);

        // Insert local as child of this
        _localCurves[i]->toggleDefaultVisualizer();
        _localCurves[i]->setCollapsed(true);
        _localCurves[i]->sample(20, 0);
        this->insert(_localCurves[i]);
        _origPoses[i] = _localCurves[i]->getPos();

        // Set varying colors to distinguish between different curves.
        double f = (i * 1.0) / (_localCurves.size() * 1.0);
        if(f < 0.25) {
            f = ((f) / 0.25);
            _localCurves[i]->setColor(GMlib::Color(0.0, f, 1.0));
        }
        else if(f < 0.5) {
            f = ((f - 0.25)) / 0.25;
            _localCurves[i]->setColor(GMlib::Color(0.0, 1.0, (1.0 - f)));
        }
        else if(f < 0.75) {
            f = ((f - 0.5)) / 0.25;
            _localCurves[i]->setColor(GMlib::Color(f, 1.0, 0.0));
        }
        else {
            f = ((f - 0.75)) / 0.25;
            _localCurves[i]->setColor(GMlib::Color(1.0, (1.0 - f), 0.0));
        }
        //_localCurves[i]->setColor(GMlib::Color(0.0, fac, 1.0 - fac));
    }
    if(_isClosed) {
        _localCurves[_numCurves - 1] = _localCurves[0];
        _origPoses[_numCurves - 1] = _localCurves[0]->getPos();
    }
    _origMat = this->getMatrix();
}

template<typename T>
void GERBSCurve<T>::setLocalCurvesVisible(bool visible)
{
    for(size_t i = 0; i < _localCurves.size(); i++) {
        _localCurves[i]->setVisible(visible);
    }
}

template<typename T>
void GERBSCurve<T>::showOffsetLocalCurves()
{
    this->setCollapsed(true);
    _animate = false;

    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _numCurves; i++) {
        _localCurves[i]->setCollapsed(false);
        _localCurves[i]->translate(GMlib::Vector<float,3>(
            0.2f * (i % 2 == 0 ? -1 : 1),
            0.0f, 0.2f * (i % 2 == 0 ? -1 : 1)));
    }
}

template<typename T>
void GERBSCurve<T>::eval(T t, int d, bool /*l*/) const
{
    this->_p.setDim(d+1);

    // Get index of knot interval
     int i = _findIndex(t);

     // Evaluate at first curve
     auto c0 = _localCurves[i - 1]->evaluateParent(t, d)[0];
     // If t == _t[i] then the curve is only affected by c0
     if(t == _t[i]) {
         this->_p[0] = c0;
         return;
     }

     // Evaluate at second curve.
     auto c1 = _localCurves[i]->evaluateParent(t, d)[0];

     // Blend curves
     auto w_i = (t - _t[i]) / (_t[i+1]-_t[i]);
     this->_p[0] = c0 * (1 - (3*pow(w_i, 2)-2*pow(w_i, 3)))
             + c1 * ((3*pow(w_i, 2)-2*pow(w_i, 3)));
}

template<typename T>
void GERBSCurve<T>::localSimulate(double dt)
{
    if(_animate) {
        // Special Effects
        switch(_curEffect) {
        case 1:
            _moveToCenter(dt); break;
        case 2:
            _moveToCircle(dt); break;
        case 3:
            _spinUp(dt); break;
        case 4:
            _alternateScale1(dt); break;
        case 5:
            _alternateScale2(dt); break;
        case 6:
            _alternateScale1(dt); break;
        case 7:
            _alternateScale2(dt); break;
        case 8:
            _alternateScale1(dt); break;
        case 9:
            _alternateScale2(dt); break;
        case 10:
            _randomStuff(dt); break;
        }
    }

    sample(200, 0);
}

template<typename T>
T GERBSCurve<T>::_w(T t, int i) const
{
    return (t - _t[i]) / (_t[i+1]-_t[i]);
}

template<typename T>
void GERBSCurve<T>::_generateKnots(int n)
{
    T start = getStartP();
    T end = getEndP();
    T dt = (end - start) / (n - 1);
    _t.resize(n+2);
    _t[0] = _t[1] = start;
    for(int i = 1; i < n - 1; i++) {
        _t[i+1] = start + dt * i;
    }
    _t[n] = _t[n+1] = end;
    if(_isClosed) {
        _t[0] = _t[1] - (_t[n] - _t[n-1]);
        _t[n+1] = _t[n] + (_t[2] - _t[1]);
    }
}

template<typename T>
int GERBSCurve<T>::_findIndex(T t) const
{
    for(int i = 1; i < _t.size() - 1; i++) {
        if(t >= _t[i] && t < _t[i + 1]) {
            return i;
        }
    }
    return _numCurves - 1;
}

template<typename T>
void GERBSCurve<T>::_nextEffect()
{
    _curEffect++;
    if(_curEffect > _numEffects) {
        _curEffect = 1;
    }
    if(_curEffect == 10) {
        _resetTransorms();
    }
    std::cout << "Cur Effect: " << _curEffect << std::endl;
}

template<typename T>
void GERBSCurve<T>::_moveToCenter(double dt)
{
    bool hasMoved = false;


    float oldScale = _localCurvesScale;
    _localCurvesScale = std::max<float>(0.01f, _localCurvesScale - dt);
    float scaleOne = _localCurvesScale / oldScale;
    GMlib::Point<float,3> scale(scaleOne, scaleOne, scaleOne);

    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _localCurves.size(); i++) {
        _localCurves[i]->scale(scale);

        auto diff = _localCurves[i]->getPos();
        auto len = diff.getLength();
        if(len > 0.0001f) {
            if(len < 0.05f) {
                _localCurves[i]->translate(-diff);
            }
            else {
                _localCurves[i]->translate(-diff * 2 * dt);
            }
            hasMoved = true;
        }
    }

    if(!hasMoved) {
        _nextEffect();
    }
}

template<typename T>
void GERBSCurve<T>::_moveToCircle(double dt)
{

    float sumDiff = 0.0f;

    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _localCurves.size(); i++) {
        float t = ((i+1) * 1.0f / _numCurves * 1.0f) * 2 * M_PI;
        GMlib::Vector<float,3> goal(6.0f*std::cos(t), 6.0f*std::sin(t), 0.0f);
        auto diff = goal - _localCurves[i]->getPos();
        auto len = diff.getLength();
        sumDiff += len;
        if(len > 0.0001f) {
            if(len < 0.05f) {
                _localCurves[i]->translate(diff);
            }
            else {
                _localCurves[i]->translate(diff * dt);
            }
        }
    }

    if(sumDiff < 1.5f) {
        _nextEffect();
    }
}

template<typename T>
void GERBSCurve<T>::_spinUp(double dt)
{
    _spinSpeed += dt;
    this->rotate(GMlib::Angle(_spinSpeed*0.5), GMlib::Vector<float,3>(0.0f, _spinSpeed / 3, (1.0 - _spinSpeed / 3)));
    if(_spinSpeed > 3.0) {
        this->setMatrix(_origMat);
        size_t i = 0;
        if(_isClosed) i++;
        for(i; i < _numCurves; i++) {
            float scale = 1.0f / _localCurvesScale;
            _localCurves[i]->scale(GMlib::Vector<float,3>(scale, scale, scale));
            _localCurves[i]->translate(_origPoses[i] - _localCurves[i]->getPos());
        }
        _localCurvesScale = 1.0f;
        _nextEffect();
    }
}

template<typename T>
void GERBSCurve<T>::_moveToOriginal(double dt)
{
    bool hasMoved = false;

    float oldScale = _localCurvesScale;
    _localCurvesScale = std::min<float>(1.0f, _localCurvesScale + dt);
    float scaleOne = _localCurvesScale / oldScale;
    GMlib::Point<float,3> scale(scaleOne, scaleOne, scaleOne);

    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _localCurves.size(); i++) {
        _localCurves[i]->scale(scale);
//        _localCurves[i]->rotate(GMlib::Angle(dt), GMlib::Vector<float,3>(1.0f, 1.0f, 0.0f));

        auto diff = _origPoses[i] - _localCurves[i]->getPos();
        auto len = diff.getLength();
        if(len > 0.0001f) {
            if(len < 0.05f) {
                _localCurves[i]->translate(diff);
            }
            else {
                _localCurves[i]->translate(diff * 2 * dt);
            }
            hasMoved = true;
        }
    }

    if(!hasMoved) {
        _nextEffect();
    }
}

template<typename T>
void GERBSCurve<T>::_alternateScale1(double dt)
{
    float oldScale1 = _altScale1;
    float oldScale2 = _altScale2;

    _altScale1 = std::min<float>(1.8f, _altScale1 + dt);
    _altScale2 = std::max<float>(0.3f, _altScale2 - dt);

    float scale1 = _altScale1 / oldScale1;
    float scale2 = _altScale2 / oldScale2;

    GMlib::Point<float,3> altScale1(scale1, scale1, scale1);
    GMlib::Point<float,3> altScale2(scale2, scale2, scale2);

    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _localCurves.size(); i += 2) {
        _localCurves[i]->scale(altScale1);
        _localCurves[i]->rotate(GMlib::Angle(dt*0.3), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
        if(i + 1 < _numCurves) {
            _localCurves[i + 1]->scale(altScale2);
            _localCurves[i + 1]->rotate(GMlib::Angle(-dt), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
        }
    }

    if(_altScale1 == 1.8f && _altScale2 == 0.3f) {
        _nextEffect();
    }
}

template<typename T>
void GERBSCurve<T>::_alternateScale2(double dt)
{
    float oldScale1 = _altScale1;
    float oldScale2 = _altScale2;

    _altScale1 = std::max<float>(0.3f, _altScale1 - dt);
    _altScale2 = std::min<float>(1.8f, _altScale2 + dt);

    float scale1 = _altScale1 / oldScale1;
    float scale2 = _altScale2 / oldScale2;

    GMlib::Point<float,3> altScale1(scale1, scale1, scale1);
    GMlib::Point<float,3> altScale2(scale2, scale2, scale2);

    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _localCurves.size(); i += 2) {
        _localCurves[i]->scale(altScale1);
        _localCurves[i]->rotate(GMlib::Angle(dt), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
        if(i + 1 < _numCurves) {
            _localCurves[i + 1]->scale(altScale2);
            _localCurves[i + 1]->rotate(GMlib::Angle(-dt*0.3), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
        }
    }

    if(_altScale2 == 1.8f && _altScale1 == 0.3f) {
        _nextEffect();
    }
}

template<typename T>
void GERBSCurve<T>::_randomStuff(double dt)
{
    GMlib::Random<int> rndInt;
    GMlib::Random<float> rndFloat;

    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _localCurves.size(); i ++) {
        // Randomly translate by x,y,z in [-0.01, 0.01]
        rndFloat.set(-0.01f, 0.01f);
        _localCurves[i]->translate(GMlib::Vector<float,3>(rndFloat.get(), rndFloat.get(), rndFloat.get()));

        // Randomly rotate by theta in [-2dt, 2dt]
        rndFloat.set(-2.0f, 2.0f);
        rndInt.set(0, 2);
        auto rotaxis = GMlib::Vector<float,3>(rndInt.get(), rndInt.get(), rndInt.get());
        if(rotaxis.getLength() == 0) rotaxis = GMlib::Vector<float,3>(0, 1, 0);
        _localCurves[i]->rotate(GMlib::Angle(rndFloat.get()*dt), rotaxis);

//        // Randomly scale by s in [0.95, 1.05]
        rndFloat.set(0.95f, 1.05f);
        rndInt.set(0, 2);
        _localCurves[i]->scale(GMlib::Vector<float,3>(rndFloat.get(), rndFloat.get(), rndFloat.get()));
    }

    _elapsed += dt;
    if(_elapsed > 2.0) _elapsed = 0.0;
    // Shift color
    // (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
    float f = _elapsed / 2.0;
    if(f < 0.25) {
        f = ((f) / 0.25);
        this->setColor(GMlib::Color(0.0, f, 1.0));
    }
    else if(f < 0.5) {
        f = ((f - 0.25)) / 0.25;
        this->setColor(GMlib::Color(0.0, 1.0, (1.0 - f)));
    }
    else if(f < 0.75) {
        f = ((f - 0.5)) / 0.25;
        this->setColor(GMlib::Color(f, 1.0, 0.0));
    }
    else {
        f = ((f - 0.75)) / 0.25;
        this->setColor(GMlib::Color(1.0, (1.0 - f), 0.0));
    }
}

template<typename T>
void GERBSCurve<T>::_resetTransorms()
{
    size_t i = 0;
    if(_isClosed) i++;
    for(i; i < _localCurves.size(); i += 2) {
        float scale = 1.0f / _altScale1;
        _localCurves[i]->scale(GMlib::Vector<float,3>(scale, scale, scale));
        _localCurves[i]->setMatrix(_origMat);
        _localCurves[i]->translate(_origPoses[i]);

        if(i + 1 < _numCurves) {
            float scale = 1.0f / _altScale2;
            _localCurves[i+1]->scale(GMlib::Vector<float,3>(scale, scale, scale));
            _localCurves[i+1]->setMatrix(_origMat);
            _localCurves[i+1]->translate(_origPoses[i+1]);
        }
    }
    _altScale1 = 1.0f;
    _altScale2 = 1.0f;
}
