#ifndef ERBSCURVE_H
#define ERBSCURVE_H

#include <parametrics/gmpcurve.h>
#include <parametrics/curves/gmpsubcurve.h>

template<typename T>
class GERBSCurve : public GMlib::PCurve<T,3>
{
    GM_SCENEOBJECT(GERBSCurve)
public:
    GERBSCurve();
    GERBSCurve(GMlib::PCurve<T,3>* curve, int n); // n = number of local curves

    bool isClosed() const override { return _isClosed; }
    void setLocalCurvesVisible(bool visible);
    void doSpecialEffects(bool doAnimate) { _animate = doAnimate; }
    void showOffsetLocalCurves();

protected:
    void eval(T t, int d = 0, bool l = false) const override;
    T getStartP() const override { return _curve->getParStart(); }
    T getEndP() const override { return _curve->getParEnd(); }

    void localSimulate(double dt) override;

private:
    GMlib::PCurve<T,3>* _curve;
    std::vector<GMlib::PCurve<T,3>*> _localCurves;
    std::vector<T> _t;
    int _numCurves;
    bool _isClosed;
    double _elapsed = 0.0;
    bool _animate = false;

    T _w(T t, int k) const;
    void _generateKnots(int n);
    int _findIndex(T t) const;

    // Special Effects
    float _localCurvesScale = 1.0f;
    float _altScale1 = 1.0f;
    float _altScale2 = 1.0f;
    int _curEffect = 1;
    int _numEffects = 10;
    double _spinSpeed = 0.0;
    std::vector<GMlib::Vector<float,3>> _origPoses;
    GMlib::HqMatrix<float,3> _origMat;

    void _nextEffect();
    void _moveToCenter(double dt);
    void _moveToCircle(double dt);
    void _spinUp(double dt);
    void _moveToOriginal(double dt);
    void _alternateScale1(double dt);
    void _alternateScale2(double dt);
    void _randomStuff(double dt);
    void _resetTransorms();
};

#include "gerbscurve.cpp"

#endif // ERBSCURVE_H
